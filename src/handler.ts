import express, {Request, Response, NextFunction} from "express";
import dotenv from "dotenv";
import serverless from "serverless-http";
import cors from "cors";
import bodyParser from "body-parser";

import {authLogin_Main} from "./tsHandler/auth/main";
import {authMiddle} from "./tsHandler/middleware/auth.middleware";

import tsUser from "./tsHandler/users/main";
import tsMovie from "./tsHandler/movies/main";

import jsUser from "./jsHandler/users/main";
import jsMovie from "./jsHandler/movies/main";

import scrap from "./scrap/main";

dotenv.config();

const app = express();

app.use(bodyParser.json({limit: "50mb"}));
app.use(cors());

// SCRAP
app.post("/Scrap", authMiddle, scrap.scrapProductByKeyword_Main);

// JS ROUTES
app.get("/JS/Users", authMiddle, jsUser.getUsers_Main);
app.get("/JS/Users/:usersId", authMiddle, jsUser.detailUsers_Main);
app.post("/JS/Users", authMiddle, jsUser.addUsers_Main);
app.put("/JS/Users/:usersId", authMiddle, jsUser.editUsers_Main);
app.put("/JS/Users-UpdateStatus/:usersId", authMiddle, jsUser.upateStatusUsers_Main);
app.delete("/JS/Users/:usersId", authMiddle, jsUser.removeUsers_Main);

app.get("/JS/MoviesSQL/:moviesId", authMiddle, jsMovie.detailMoviesSQLInject_Main);
app.get("/JS/Movies", authMiddle, jsMovie.getMovies_Main);
app.get("/JS/Movies/:moviesId", authMiddle, jsMovie.detailMovies_Main);
app.post("/JS/Movies", authMiddle, jsMovie.addMovies_Main);
app.put("/JS/Movies/:moviesId", authMiddle, jsMovie.editMovies_Main);
app.put("/JS/Movies-UpdateStatus/:moviesId", authMiddle, jsMovie.upateStatusMovies_Main);
app.delete("/JS/Movies/:moviesId", authMiddle, jsMovie.removeMovies_Main);

// TS ROUTES
app.get("/TS/MoviesSQL/:moviesId", authMiddle, tsMovie.detailMoviesSQLInject_Main);
app.get("/TS/Movies", authMiddle, tsMovie.getMovies_Main);
app.get("/TS/Movies/:moviesId", authMiddle, tsMovie.detailMovies_Main);
app.post("/TS/Movies", authMiddle, tsMovie.addMovies_Main);
app.put("/TS/Movies/:moviesId", authMiddle, tsMovie.editMovies_Main);
app.put("/TS/Movies-UpdateStatus/:moviesId", authMiddle, tsMovie.upateStatusMovies_Main);
app.delete("/TS/Movies/:moviesId", authMiddle, tsMovie.removeMovies_Main);

app.get("/TS/Users", authMiddle, tsUser.getUsers_Main);
app.get("/TS/Users/:usersId", authMiddle, tsUser.detailUsers_Main);
app.post("/TS/Users", authMiddle, tsUser.addUsers_Main);
app.put("/TS/Users/:usersId", authMiddle, tsUser.editUsers_Main);
app.put("/TS/Users-UpdateStatus/:usersId", authMiddle, tsUser.upateStatusUsers_Main);
app.delete("/TS/Users/:usersId", authMiddle, tsUser.removeUsers_Main);

app.post("/TS/Auth-signin", authLogin_Main);

app.get("/", (req: Request, res: Response) => {
  return res.status(404).json({
    error: "Not Found",
  });
});

export const handler = serverless(app);
