// list, detail, add
"use strict";

const PrismaClient = require("@prisma/client");
const prisma = new PrismaClient.PrismaClient();

const IResponse = {
  message: String,
  error: Boolean,
  data: [],
};

let response = {
  message: "ok",
  error: false,
  data: [],
};

/**
 * getProduct_Module
 * @function getProduct_Module module for get
 * @returns {Promise<{error: boolean,message:string, data:any }>}
 */
export async function getProduct_Module() {
  try {
    let result = []; // result for prisma
    result = await prisma.scrap.findMany({
      where: {
        AND: [
          {
            "status": "active",
          },
        ],
      },
    });
    response.data = result;
  } catch (e) {
    console.error(e.meta, "::getScrap_Module");
    response.error = true;
    response.message = "failed get data";
  }
  return {...response};
}

/**
 * detailProduct_Module
 * @function detailProduct_Module module for get detail productId
 * @param {number} productId id required
 * @returns {Promise<{error: boolean,message:'ok|success', data:any }>}
 */
export async function detailProduct_Module(productId = number) {
  try {
    let result = []; // result for prisma
    result = await prisma.scrap.findMany({
      where: {
        AND: [
          {
            id: productId,
          },
          {
            "status": "active",
          },
        ],
      },
    });

    response.data = result; // store to response data
  } catch (e) {
    console.error(e.meta, "::detailProduct_Module");
    response.error = true;
    response.message = "failed get detail data";
  }

  return {...response};
}

/**
 * addProduct_Module
 * @function addProduct_Module module for add
 * @param {object} payload
 * @returns {Promise<{message: string, error: boolean, data: any[]}>}
 */
export async function addProduct_Module(payload) {
  let result = response;
  try {
    const lada = genSaltSync(10);
    const hash = hashSync(payload.password, lada);

    result.data = await prisma.scrap.create({
      data: payload,
    });
  } catch (e) {
    console.error(e, "::addProduct_Module");
    response.error = true;
    response.message = "failed add data";
  }
  return {...response};
}
