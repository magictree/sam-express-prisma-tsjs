"use strict";

import {
  listProduct_Service,
  detailProduct_Service,
  scrapProductByKeyword_Service,
} from "./scrap.service";

/**
 * @function getProduct_Main  function for get list
 * @method GET
 * @param {object} request Request data express
 * @param {object} response Response data express
 * @param {object} next Callback data express
 * @returns {Promise<*>} API Response { message, error, data }
 */
async function getProduct_Main(request, response, next) {
  console.log("JS::getProduct_Main");
  const result = await listProduct_Service();
  return response.json(result);
}

/**
 * @function detailProduct_Main  function for get detail by id
 * @method GET
 * @param {object} request Request data express
 * @param {object} response Response data express
 * @param {object} next Callback data express
 * @returns {Promise<*>} API Response { message, error, data }
 */
async function detailProduct_Main(request, response, next) {
  console.log("JS::detailProduct_Main");
  const result = await detailProduct_Service({usersId: request.params.usersId});
  return response.status(result.error ? 400 : 200).json(result);
}

/**
 * @function scrapProductByKeyword_Main  function for add
 * @method POST
 * @param {object} request Request data express
 * @param {object} response Response data express
 * @param {object} next Callback data express
 * @returns {Promise<*>} API Response { message, error, data }
 */
async function scrapProductByKeyword_Main(request, response, next) {
  console.log("JS::scrapProductByKeyword_Main");
  const result = await scrapProductByKeyword_Service(request.body);
  return response.status(result.error ? 400 : 200).json(result);
}

export default {
  getProduct_Main,
  detailProduct_Main,
  scrapProductByKeyword_Main,
};
