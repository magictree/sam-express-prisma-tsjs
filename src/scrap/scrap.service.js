const puppeteer = require("puppeteer");
// import {addProduct_Module, detailProduct_Module, getProduct_Module} from "./scrap.module";

require("dotenv").config();

let response = {
  message: "ok",
  error: false,
  data: [],
};

/**
 * listProduct_Service
 * @function ListProduct_Service services get
 * @returns {Promise<{error: boolean,message:string, data:object }>}
 */
// export async function listProduct_Service() {
//   try {
//     let res = null;
//     res = await getProduct_Module();
//     return {...res};
//   } catch (error) {
//     return {error: true, message: error, data: []};
//   }
// }

/**
 * detailProduct_Service
 * @function detailProduct_Service
 * @param {object} payload required
 * @param {number} payload.productId required
 * @return {Promise<{error: boolean,message:'ok|success', data:object }>}
 */
// export async function detailProduct_Service(payload) {
//   try {
//     res = await detailProduct_Module({...res.data.productId});
//     if (res.data.length) {
//       res.data = res.data[0];
//     } else {
//       res.message = "Data not found";
//     }
//     return {...res};
//   } catch (error) {
//     return {error: true, message: error, data: []};
//   }
// }

/**
 * scrapProductByKeyword_Service
 * @function addUsers_Service services add
 * @param {object} payload
 * @param {string} payload.keyword
 * @returns {Promise<{error: boolean,message:string, data:any }>}
 */
exports.scrapProductByKeyword_Service = async (payload) => {
  try {
    let url = encodeURI(`${process.env.URLSHOOPE}`);
    let browser = await puppeteer.launch({
      headless: "new",
    });

    browser.userAgent(
      "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36"
    );
    let page = await browser.newPage();

    await page.setExtraHTTPHeaders({
      // "user-agent":
      //   "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36",
      //"upgrade-insecure-requests": "1",
      "Accept":
        "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8",
      //"accept-encoding": "gzip, deflate, br",
      //"accept-language": "en-US,en;q=0.6",
      // "Upgrade-Insecure-Requests": "1",
      //"Cache-Control": "max-age=0",
    });
    await page.goto(url, {
      waitUntil: "networkidle2",
      referer: url,
    });
    await page.waitForNetworkIdle();
    await page.evaluate(async () => {
      let main = document
        .querySelector("#main shopee-banner-popup-stateful")
        .shadowRoot.querySelector("div.home-popup__close-area div.shopee-popup__close-btn");
      main.click(true);
      // // typing keyword
      // let Isearch = document.querySelector("input[class='shopee-searchbar-input__input']");
      // Isearch.value = "Asus";

      // document.ke;
      // // click button search
      // let Bsearch = document.querySelector(
      //   "button[class='btn btn-solid-primary btn--s btn--inline shopee-searchbar__search-button']"
      // );
      // Bsearch.click(true);
      // await page.click(
      //   "button[class='btn btn-solid-primary btn--s btn--inline shopee-searchbar__search-button']"
      // );

      // #### next, Focus get list product with limit 3 ####
      // let limit = 2; // default
      // let sectionListProductNodes = document.querySelector(
      //   "ul[class='row shopee-search-item-result__items']"
      // );
      // let arrListProductNodes = sectionListProductNodes.childNodes.slice(0, limit);
      // console.log(arrListProductNodes);
    });
    // await page.keyboard.down("Enter");
    // await page.keyboard.up("Enter");
    await page.type("input[class='shopee-searchbar-input__input']", "mackbook", {delay: 500});

    setTimeout(async () => {
      await page.click(
        "button[class='btn btn-solid-primary btn--s btn--inline shopee-searchbar__search-button']"
      );
    }, 200);
    setTimeout(async () => {
      await page.screenshot({
        path: "screenshot2.jpg",
      });
    }, 5000);
    //res = await addProduct_Module(payload);
    // return {error: false, message: "ok", data: res || ""};
  } catch (error) {
    console.log(error, "::scrapProductByKeyword_Service");
    return {error: true, message: error, data: []};
  }
};
