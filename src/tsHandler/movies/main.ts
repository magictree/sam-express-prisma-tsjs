"use strict";

require("dotenv").config();

import {NextFunction, Request, Response} from "express";

import {
  detailMoviesSQLInject_Service,
  getMovies_Service,
  detailMovies_Service,
  addMovies_Service,
  editMovies_Service,
  updateStatusMovies_Service,
  removeMovies_Service,
} from "./movies.service";

/**
 * @function getMovies_Main  function for get movies list
 * @method GET
 * @param {object} request Request data express
 * @param {object} response Response data express
 * @param {object} next Callback data express
 * @returns {Promise<*>} API Response { message, error, data }
 */
async function getMovies_Main(
  request: Request,
  response: Response,
  next: NextFunction
): Promise<any> {
  console.log("TS::getMovies_Main");
  const result = await getMovies_Service();
  return response.json(result);
}

/**
 * @function detailMovies_Main  function for get detail movies by id
 * @method GET
 * @param {object} request Request data express
 * @param {object} response Response data express
 * @param {object} next Callback data express
 * @returns {Promise<*>} API Response { message, error, data }
 */
async function detailMovies_Main(
  request: Request,
  response: Response,
  next: NextFunction
): Promise<any> {
  console.log("TS::detailMovies_Main");
  const result = await detailMovies_Service({moviesId: request.params.moviesId});
  return response.status(result.error ? 400 : 200).json(result);
}

/**
 * @function detailMoviesSQLInject_Main  function for get detail movies by id
 * @method GET
 * @param {object} request Request data express
 * @param {object} response Response data express
 * @param {object} next Callback data express
 * @returns {Promise<*>} API Response { message, error, data }
 */
async function detailMoviesSQLInject_Main(
  request: Request,
  response: Response,
  next: NextFunction
): Promise<any> {
  console.log("TS::detailMoviesSQL_Main");
  const result = await detailMoviesSQLInject_Service(request.params.moviesId);
  return response.status(result.error ? 400 : 200).json(result);
}

/**
 * @function addMovies_Main  function for add movies
 * @method POST
 * @param {object} request Request data express
 * @param {object} response Response data express
 * @param {object} next Callback data express
 * @returns {Promise<*>} API Response { message, error, data }
 */
async function addMovies_Main(request: any, response: Response, next: NextFunction): Promise<any> {
  console.log("TS::addMovies_Main");
  const body = {...request.body, authorId: request.token.id};
  const result = await addMovies_Service(body);
  return response.status(result.error ? 400 : 200).json(result);
}

/**
 * @function editMovies_Main  function for edit movies by id
 * @method PUT
 * @param {object} request Request data express
 * @param {object} response Response data express
 * @param {object} next Callback data express
 * @returns {Promise<*>} API Response { message, error, data }
 */
async function editMovies_Main(
  request: Request,
  response: Response,
  next: NextFunction
): Promise<any> {
  console.log("TS::editMovies_Main");
  let data = {...request.params, ...request.body};
  const result = await editMovies_Service(data);
  return response.status(result.error ? 400 : 200).json(result);
}

/**
 * @function upateStatusMovies_Main  function for update status movies
 * @method PUT
 * @param {object} request Request data express
 * @param {object} response Response data express
 * @param {object} next Callback data express
 * @returns {Promise<*>} API Response { message, error, data }
 */
async function upateStatusMovies_Main(
  request: Request,
  response: Response,
  next: NextFunction
): Promise<any> {
  console.log("TS::upateStatusMovies_Main");
  let data = {...request.params, ...request.body};
  const result = await updateStatusMovies_Service(data);
  return response.status(result.error ? 400 : 200).json(result);
}

/**
 * @function removeMovies_Main  function for remove movies
 * @method DELETE
 * @param {object} request Request data express
 * @param {object} response Response data express
 * @param {object} next Callback data express
 * @returns {Promise<*>} API Response { message, error, data }
 */
async function removeMovies_Main(
  request: Request,
  response: Response,
  next: NextFunction
): Promise<any> {
  console.log("TS::removeMovies_Main");
  const result = await removeMovies_Service({moviesId: request.params.moviesId});
  return response.status(result.error ? 400 : 200).json(result);
}

export default {
  getMovies_Main,
  detailMovies_Main,
  addMovies_Main,
  editMovies_Main,
  upateStatusMovies_Main,
  removeMovies_Main,
  detailMoviesSQLInject_Main,
};
