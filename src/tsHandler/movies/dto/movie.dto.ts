export interface CreateMovieDTO {
  title: string;
  description: string | null;
  rating: number;
  image: string | null;
  status: string | null;
  authorId: number | null;
}

export interface EditMovieDTO {
  moviesId: number | null;
  title: string;
  description: string | null;
  rating: number;
  image: string | null;
  status: string | null;
  authorId: number | null;
}

export interface UpdateStatusMovieDTO {
  moviesId: number | null;
  status: string | null;
}

export interface ByIdMovieDTO {
  moviesId: number | string;
}

export interface DetailMovieDTO {
  moviesId: number | string;
}
