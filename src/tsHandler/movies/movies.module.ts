"use strict";

const PrismaClient = require("@prisma/client");
import {CreateMovieDTO, EditMovieDTO} from "./dto/movie.dto";
const prisma = new PrismaClient.PrismaClient();

interface IResponse {
  message: string;
  error: boolean;
  data: any[];
}

let response: IResponse = {
  message: "ok",
  error: false,
  data: [],
};

/**
 * getMovies_Module
 * @function getMovies_Module module for get movies
 * @returns {Promise<{error: boolean,message:string, data:any }>}
 */
export async function getMovies_Module(): Promise<IResponse> {
  try {
    let result = []; // result for prisma
    result = await prisma.movie.findMany({
      where: {
        AND: [
          {
            "status": "active",
          },
        ],
      },
      select: {
        id: true,
        title: true,
        description: true,
        rating: true,
        author: {
          select: {
            id: true,
            fullname: true,
          },
        },
      },
    });
    response.data = result; // store to response data
  } catch (e) {
    console.error(e.meta, "::getMovies_Module");
    response.error = true;
    response.message = "failed get data";
  }
  return {...response};
}

/**
 * detailMovies_Module
 * @function detailMovies_Module module for get detail movies
 * @param {number} moviesId required
 * @returns {Promise<{error: boolean,message:'ok|success', data:any }>}
 */
export async function detailMovies_Module(moviesId: number): Promise<IResponse> {
  try {
    let result = []; // result for prisma
    result = await prisma.movie.findMany({
      where: {
        AND: [
          {
            id: moviesId,
          },
          {
            "status": "active",
          },
        ],
      },
      select: {
        id: true,
        title: true,
        description: true,
        rating: true,
        author: {
          select: {
            id: true,
            fullname: true,
          },
        },
      },
    });

    response.data = result; // store to response data
  } catch (e) {
    console.error(e.meta, "::detailMovies_Module");
    response.error = true;
    response.message = "failed get detail data";
  }

  return {...response};
}

/**
 * detailMoviesSQLInject_Module
 * @function detailMoviesSQLInject_Module module for get detail movies
 * @param {number} moviesId required
 * @returns {Promise<{error: boolean,message:'ok|success', data:any }>}
 */
export async function detailMoviesSQLInject_Module(moviesId: string): Promise<IResponse> {
  try {
    let result = []; // result for prisma
    result = await prisma.$queryRawUnsafe(
      `SELECT id, fullname, email from user where id=${moviesId}`
    );

    console.log(result, "RESULT");

    response.data = result; // store to response data
  } catch (e) {
    console.error(e.meta, "::detailMoviesSQLInject_Module");
    response.error = true;
    response.message = "failed get detail data";
  }

  return {...response};
}

/**
 * addMovies_Module
 * @function addMovies_Module module for add movies
 * @param {object} payload
 * @param {string} payload.title
 * @param {string} payload.description allow null
 * @param {number} payload.rating default 0
 * @param {string} payload.image default ''
 * @param {number} payload.authorId is required
 * @param {string} payload.status optional default active
 * @example * var payload = { title: "Transformers", description: "This description movie", rating:8.0, image:"path/bucket", authorId:1 }; *
 * @returns {Promise<{message: string, error: boolean, data: any[]}>}
 */
export async function addMovies_Module(payload: CreateMovieDTO): Promise<IResponse> {
  let result: any = [];
  try {
    result = await prisma.movie.create({
      data: {
        title: payload.title,
        description: payload.description,
        rating: payload.rating,
        image: payload.image,
        authorId: payload.authorId,
      },
    });
    response.data = result;
  } catch (e) {
    console.error(e, "::addMovies_Module");
    response.error = true;
    response.message = "failed add data";
  }
  return {...response};
}

/**
 * editMovies_Module
 * @function editMovies_Module module for edit movies
 * @param {object} payload
 * @param {string} payload.moviesId is required
 * @param {string} payload.title
 * @param {string} payload.description allow null
 * @param {number} payload.rating default 0
 * @param {string} payload.image default ''
 * @example * var payload = { moviesId:123, title: "Transformers", description: "This description movie", rating:8.0, image:"path/bucket" }; *
 * @returns {Promise<{message: string, error: boolean, data: any}>}
 */
export async function editMovies_Module(payload: EditMovieDTO): Promise<IResponse> {
  let result: any = [];
  try {
    const ID = payload.moviesId;
    result = await prisma.movie.update({
      data: {
        title: payload.title,
        description: payload.description,
        rating: payload.rating,
        image: payload.image,
        authorId: payload.authorId,
      },
      where: {
        id: ID,
      },
    });
    response.data = result;
  } catch (e) {
    console.error(e.meta, "::editMovies_Module");
    response.error = true;
    response.message = "Data not found";
  }
  return {...response};
}

/**
 * updateStatusMovies_Module
 * @function updateStatusMovies_Module module for update status movies
 * @param {object} payload
 * @param {string} payload.moviesId is required
 * @param {string} payload.status optional default active
 * @example * var payload = { moviesId: "1234", status:'active|inactive' }; *
 * @returns {Promise<{error: boolean,message:'ok|success', data:object }>}
 */
export const updateStatusMovies_Module = async function (payload: {
  moviesId: number;
  status: "active" | "inactive";
}): Promise<IResponse> {
  try {
    let result: any = [];
    const id = payload.moviesId;

    result = await prisma.movie.update({
      data: {
        status: payload.status,
      },
      where: {id},
    });
    response.data = result;
  } catch (e) {
    console.error(e.meta, "::updateStatusMovies_Module");
    response.error = true;
    response.message = "data not found";
  }
  return {...response};
};
