"use strict";

import {
  addMovies_Spec,
  editMovies_Spec,
  detailMovies_Spec,
  updateStatusMovies_Spec,
  removeMovies_Spec,
} from "./movies.spec";
import {
  detailMoviesSQLInject_Module,
  getMovies_Module,
  detailMovies_Module,
  addMovies_Module,
  editMovies_Module,
  updateStatusMovies_Module,
} from "./movies.module";

import {
  DetailMovieDTO,
  CreateMovieDTO,
  EditMovieDTO,
  UpdateStatusMovieDTO,
  ByIdMovieDTO,
} from "./dto/movie.dto";

let IResponse = {
  message: String,
  error: Boolean,
  data: Array,
};

let response = (IResponse = {
  message: "ok",
  error: false,
  data: [],
});

/**
 * getMovies_Service
 * @function getMovies_Service services get movies
 * @returns {Promise<{error: boolean,message:string, data:object }>}
 */
export async function getMovies_Service() {
  let res = response;
  try {
    res = await getMovies_Module();
  } catch (error) {
    res.message = "Bad request";
    res.error = true;
  }
  return res;
}

/**
 * detailMovies_Service
 * @function detailMovies_Service services detail movies
 * @param {string} moviesId required
 * @returns {Promise<{error: boolean,message:'ok|success', data:object }>}
 */
export async function detailMovies_Service(payload = DetailMovieDTO) {
  let res = response;
  try {
    res = await detailMovies_Spec(payload);
    if (!res.error) {
      res = await detailMovies_Module({...res.data.moviesId});
      if (res.data.length) {
        res.data = res.data[0];
      } else {
        res.message = "Data not found";
      }
    }
  } catch (error) {
    res.message = "Bad request";
    res.error = true;
  }
  return res;
}

/**
 * detailMoviesSQLInject_Service
 * @function detailMoviesSQLInject_Service services detail movies
 * @param {string} moviesId required
 * @returns {Promise<{error: boolean,message:string, data:any }>}
 */
export async function detailMoviesSQLInject_Service(moviesId = String) {
  let res = response;
  try {
    res = await detailMoviesSQLInject_Module(moviesId);
    if (res.data.length) {
      res.data = res.data;
    } else {
      res.message = "Data not found";
    }
  } catch (error) {
    res.message = "Bad request";
    res.error = true;
  }
  return res;
}

/**
 * addMovies_Service
 * @function addMovies_Service services add item to movies
 * @param {object} payload
 * @param {string} payload.title
 * @param {string} payload.description allow null
 * @param {number} payload.rating default 0
 * @param {string} payload.image default ''
 * @param {string} payload.status optional default active
 * @example * var payload = { title: "Transformers", description: "This description movie", rating:8.0, image:"path/bucket" }; *
 * @returns {Promise<{error: boolean,message:string, data:any }>}
 */
export async function addMovies_Service(payload = CreateMovieDTO) {
  let res = response;
  try {
    res = await addMovies_Spec(payload);
    if (!res.error) {
      res = await addMovies_Module(payload);
    }
    return res;
  } catch (error) {
    res.message = "Bad request";
    res.error = true;
    return res;
  }
}

/**
 * editMovies_Service
 * @function editMovies_Service services edit course
 * @param {object} payload
 * @param {string} payload.moviesId is required
 * @param {string} payload.title
 * @param {string} payload.description allow null
 * @param {number} payload.rating default 0
 * @param {string} payload.image default ''
 * @param {string} payload.status optional default active
 * @example * var payload = { moviesId:12, title: "Transformers", description: "This description movie", rating:8.0, image:"path/bucket" }; *
 * @returns {Promise<{error: boolean,message:'ok|success', data:object }>}
 */
export async function editMovies_Service(payload = EditMovieDTO) {
  let res = response;
  try {
    let res = await editMovies_Spec(payload);
    if (!res.error) {
      console.log(13);
      res = await editMovies_Module(res.data);
    }
    return res;
  } catch (error) {
    res.message = "Bad request";
    res.error = true;
    return res;
  }
}

/**
 * updateStatusMovies_Service
 * @function updateStatusMovies_Service services udpate status course
 * @param {object} payload
 * @param {string} payload.moviesId is required
 * @param {string} payload.status optional default active
 * @example * var payload = { moviesId:213123,, status:"inactive" }; *
 * @returns {Promise<{error: boolean,message:'ok|success', data:object }>}
 */
export async function updateStatusMovies_Service(payload = UpdateStatusMovieDTO) {
  let res = response;
  try {
    res = await updateStatusMovies_Spec(payload);

    if (!res.error) {
      res = await updateStatusMovies_Module(res.data);
    }
    return res;
  } catch (error) {
    res.message = "Bad request";
    res.error = true;
    return res;
  }
}

/**
 * removeMovies_Service
 * @function removeMovies_Service services remove movies
 * @param {object} payload
 * @param {string} payload.moviesId is required
 * @example * var payload = { moviesId:213123 }; *
 * @returns {Promise<{error: boolean,message:'ok|success', data:any }>}
 */
export async function removeMovies_Service(payload = ByIdMovieDTO) {
  let res = response;
  try {
    let res = await removeMovies_Spec(payload);
    if (!res.error) {
      res = await updateStatusMovies_Module(res.data);
    }
    return res;
  } catch (error) {
    res.message = "Bad request";
    res.error = true;
    return res;
  }
}
