import Joi from "joi";
import {CreateMovieDTO, EditMovieDTO, DetailMovieDTO} from "./dto/movie.dto";

const item = {
  moviesId: Joi.number().positive().required(),
  title: Joi.string().invalid("", null).required(),
  description: Joi.string().allow(null),
  rating: Joi.number().default(0).min(0).required(),
  image: Joi.string().default(""),
  authorId: Joi.number().positive().required(),
  status: Joi.string().allow("active", "inactive").default("active"),
};
const schemaCreate = Joi.object()
  .keys({
    title: item.title,
    description: item.description,
    rating: item.rating,
    image: item.image,
    authorId: item.authorId,
    status: item.status,
  })
  .required();

const schemaEdit = Joi.object()
  .keys({
    moviesId: item.moviesId,
    title: item.title,
    description: item.description,
    rating: item.rating,
    image: item.image,
  })
  .required();

const schemaStatusItem = Joi.object().keys({moviesId: item.moviesId, status: item.status});

const schemaID = Joi.object().keys({moviesId: item.moviesId});

/**
 * addMovies_Spec
 * add movies
 * @param {object} payload
 * @param {string} payload.title
 * @param {string} payload.description allow null
 * @param {number} payload.rating default 0
 * @param {string} payload.image default ''
 * @param {string} payload.status optional default active
 * @example * var payload = { title: "Transformers", description: "This description movie", rating:8.0, image:"path/bucket" }; *
 * @returns {Promise<{message: string ,error: boolean, data: object}>}
 */
export async function addMovies_Spec(payload = CreateMovieDTO) {
  let {value, error} = schemaCreate.validate(payload);
  value = {
    ...value,
    rating: parseFloat(value.rating),
  };
  return {
    message: error ? error.details[0].message : "ok",
    error: error ? true : false,
    data: value,
  };
}

/**
 * editMovies_Spec
 * @param {object} payload
 * @param {string} payload.moviesId
 * @param {string} payload.title
 * @param {string} payload.description allow null
 * @param {number} payload.rating default 0
 * @param {string} payload.image default ''
 * @param {string} payload.status optional default active
 * @example * var payload = { title: "Transformers", description: "This description movie", rating:8.0, image:"path/bucket" }; *
 * @returns {Promise<{message: string ,error: boolean, data: object}>}
 */
export async function editMovies_Spec(payload = EditMovieDTO) {
  let {value, error} = schemaEdit.validate(payload);
  value = {
    ...value,
    rating: parseFloat(value.rating),
  };
  return {
    message: error ? error.details[0].message : "ok",
    error: error ? true : false,
    data: value,
  };
}

/**
 * updateStatusMovies_Spec
 * update Status movies by id
 * @param {object} payload
 * @param {string} payload.moviesId is required
 * @param {string} payload.status is required
 * @example * var payload = { moviesId: 1234, status: 'active|inactive' }; *
 * @returns {Promise<{message: string, error: boolean, data: any}>}
 */
export async function updateStatusMovies_Spec(payload) {
  let {value, error} = schemaStatusItem.validate(payload);
  value = {
    ...value,
    moviesId: Number(value.moviesId),
  };
  return {
    message: error ? error.details[0].message : "ok",
    error: error ? true : false,
    data: value,
  };
}

/**
 * detailMovies_Spec
 * get all movies or by id
 * @param {object} payload
 * @param {string} payload.moviesId optional
 * @returns {Promise<{message: string, error: boolean, data: object}>}
 */
export async function detailMovies_Spec(payload = DetailMovieDTO) {
  let {value, error} = schemaID.validate(payload);
  value = {
    moviesId: Number(value.moviesId),
  };
  return {
    message: error ? error.details[0].message : "ok",
    error: error ? true : false,
    data: value,
  };
}

/**
 * removeMovies_Spec
 * get all movies or by id
 * @param {object} payload is required
 * @param {string} payload.moviesId is required
 * @returns {Promise<{message: string, error: boolean, data: object}>}
 */
export async function removeMovies_Spec(payload) {
  let {value, error} = schemaID.validate(payload);
  value = {
    moviesId: Number(value.moviesId),
    status: "inactive",
  };
  return {
    message: error ? error.details[0].message : "ok",
    error: error ? true : false,
    data: value,
  };
}
