exports.CreateMovieDTO = {
  title: String,
  description: String,
  rating: Number,
  image: "",
  status: String,
  authorId: Number,
};

exports.EditMovieDTO = {
  moviesId: Number,
  title: String,
  description: String,
  rating: Number,
  image: "",
  status: String,
  authorId: Number,
};

exports.UpdateStatusMovieDTO = {
  moviesId: Number,
  status: String,
};

exports.ByIdMovieDTO = {
  moviesId: Number,
};

exports.DetailMovieDTO = {
  moviesId: Number,
};
