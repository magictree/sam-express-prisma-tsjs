import {PrismaClient} from "@prisma/client";

const prisma = new PrismaClient();

async function main() {
  await prisma.user.createMany({
    data: [
      {
        id: 1,
        fullname: "admin",
        email: "admin@email.com",
        password: "$2b$10$RSqIk2GdtGM/Lrd4hOIFy.V3kg3gvbvpWm647GU3QXK4N1oY7o3zS",
        token: null,
      },
    ],
  });
  await prisma.movie.createMany({
    data: [
      {
        title: "Pengabdi Setan 2 Comunion",
        description:
          "adalah sebuahh film horor indonesia tahun 2022 yang disutradarai dan ditulis oleh Joko Anwar sebagai sekuel tahun 2017, Pengabdi Setan.",
        rating: 7.0,
        image: "",
        authorId: 1,
      },
      {
        title: "Pengabdi Setan",
        description:
          "adalah sebuahh film horor indonesia tahun 2017 yang disutradarai dan ditulis oleh Joko Anwar",
        rating: 8.0,
        image: "",
        authorId: 1,
      },
    ],
  });
}

main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
